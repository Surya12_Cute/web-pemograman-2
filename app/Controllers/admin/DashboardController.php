<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;


class DashboardController extends BaseController
{
	public function index()
	{
		return view('admin/dashboard');
	}

}

