<?php

namespace App\Controllers\admin\marketings;


use App\Models\marketings;
use CodeIgniter\I18n\Time;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;


class MarketingsController extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
		$this->marketings = new marketings();
    }

	public function index()
	{
		if ($this->request->isAJAX()){

			$marketings = $this->marketings->select([
				"marketings.id",
				"marketings.code",
				"marketings.name",
				"users.email"
			])->join('users', 'marketings.id = users.marketing_id', "LEFT")
			->findAll();


			foreach ($marketings as $key => $marketing) {
				$marketing->action = view('admin/marketing/datatables/action', [
					"id" => $marketing->id
				]);
			}
			
			return json_encode([
				'data' => $marketings
			]);

		}
		
		return view('admin/marketing/index');
	}

	
	public function select_form()
	{
		if ($this->request->isAJAX()){

			$marketings = $this->marketings->select([
				"id",
				"name as text"
			]);
			

			if($this->request->getVar('search')){
				$marketings = $marketings
				->like('code', $this->request->getVar('search'))
				->orLike('name', $this->request->getVar('search'));
			}


			$marketings = $marketings->findAll();

			return json_encode($marketings);
		}
	}

	

	public function form_valid_rules()
	{
		return [
			'code' => [
				"label" => "code",
				"rules" => "required",
				"errors" => [
					"required" => "Kode Marketing Wajib Diisi."
				],
			],
			'name' => [
				"label" => "name",
				"rules" => "required",
				"errors" => [
					"required" => "Nama Marketing Wajib Diisi."
				],
			],
			'email' => [
				"label" => "email",
				"rules" => "required|valid_email",
				"errors" => [
					"required" => "Email Marketing Wajib Diisi.",
					"valid_email" => "Email Tidak Benar.",
				],
			],
			'birth_date' => [
				"label" => "birth_date",
				"rules" => "required|valid_date[d/m/Y]",
				"errors" => [
					"required" => "Tanggal Lahir Wajib Diisi.",
					"valid_date" => "Tanggal Lahir Kurang Lengkap."
				]
			],
			'role_id' => [
				"label" => "role_id",
				"rules" => "required",
				"errors" => [
					"required" => "Silahkan Pilih Jabatan."
				]
			]
		];
	}
	
	public function store()
	{

		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = [
			'code' => $this->request->getVar('code'),
			'name' => $this->request->getVar('name'),
			'email' => $this->request->getVar('email'),
			'password' => $this->request->getVar('password'),
			'birth_place' => $this->request->getVar('birth_place'),
			'gender' => $this->request->getVar('gender'),
			'phone_number' => $this->request->getVar('phone_number'),
			"role_id" => $this->request->getVar('role_id')
		];
		
		$birth_date = Time::createFromFormat('j/m/Y', $this->request->getVar('birth_date'), 'Asia/Jakarta');
		$data['birth_date'] = $birth_date->toDateString();
		
		
		$marketings = $this->marketings->CreateMarketingUser($data);


		return $this->respondCreated($marketings);
	}

	
	public function edit($id)
	{
		$marketings = $this->marketings->select([
			"marketings.code",
			"marketings.name",
			"marketings.phone_number",
			"users.email",
			"DATE_FORMAT(marketings.birth_date, '%d/%m/%Y') as birth_date",
			"marketings.birth_place",
			"marketings.gender",
			"auth_groups_users.group_id as role_id",
			"auth_groups.name as role_name"
		])
		->join('users', 'marketings.id = users.marketing_id', "LEFT")
		->join('auth_groups_users', 'users.id = auth_groups_users.user_id', "LEFT")
		->join('auth_groups', 'auth_groups_users.group_id = auth_groups.id', "LEFT")
		->find($id);

		return $this->respondCreated($marketings);
	}
	
	public function update($id)
	{
		
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();
		
		$birth_date = Time::createFromFormat('j/m/Y', $this->request->getVar('birth_date'), 'Asia/Jakarta');
		$data->birth_date = $birth_date->toDateString();

		$marketings = $this->marketings->UpdateMarketingUser($id, $data);
		

		return $this->respondCreated($marketings);
	}
	
	public function delete($id)
	{	
		$marketings = $this->marketings->delete($id);
		

		return $this->respondCreated($marketings);
	}
	
}

