<?php

namespace App\Controllers\admin\products;

use App\Models\products;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Faker\Factory;
use Config\Services;


class ProductsController extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
		$this->Generator = Factory::create();
		$this->products = new products();
    }

	public function index()
	{
		if ($this->request->isAJAX()){

			$products = $this->products->select([
				"products.id",
				"products.code",
				"products.name",
				"products.price",
				"products.image",
				"product_categories.name as category",
				"product_brands.image as brand_image"
			])
			->join('product_categories', 'product_categories.id = products.category_id', "LEFT")
			->join('product_brands', 'product_brands.id = products.brand_id', "LEFT")
			->findAll();

			foreach ($products as $key => $product) {
				$products[$key]['action'] = view('admin/product/datatables/action', $product);
				$products[$key]['price'] = rupiah($products[$key]['price']);

				$Pimage =  $products[$key]['image'];
				if($Pimage){	
					$products[$key]['image'] =  "<img src='". product_url($Pimage) ."' width='60px'>";
				}

				$Brandimage =  $products[$key]['brand_image'];
				if($Brandimage){	
					$products[$key]['brand_image'] =  "<img src='". brand_url($Brandimage) ."' width='60px'>";
				}
			}
			
			return json_encode([
				'data' => $products
			]);

		}
		
		return view('admin/product/index');
	}

	
	public function select_form()
	{
		if ($this->request->isAJAX()){

			$products = $this->products->select([
				"id",
				"name as text"
			]);
			

			if($this->request->getVar('search')){
				$products = $products
				->like('code', $this->request->getVar('search'))
				->orLike('name', $this->request->getVar('search'));
			}


			$products = $products->findAll();

			return json_encode($products);
		}
	}


	
	public function form_valid_rules()
	{
		return [
			'code' => [
				"label" => "code",
				"rules" => "required",
				"errors" => [
					"required" => "Kode Customer Wajib Diisi."
				],
			],
			'name' => [
				"label" => "name",
				"rules" => "required",
				"errors" => [
					"required" => "Nama Customer Wajib Diisi."
				],
			],
			'category_id' => [
				"label" => "category_id",
				"rules" => "required",
				"errors" => [
					"required" => "Silahkan Pilih Produk Kategori Terlebih Dahulu.",
				],
			],
			'price' => [
				"label" => "price",
				"rules" => "required|greater_than[0]",
				"errors" => [
					"required" => "Harga Produk Wajib Diisi.",
					"greater_than" => "Harga Harus Lebih dari 0.",
				],
			]
		];
	}
	
	public function store()
	{
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}
		

		$data = $this->request->getVar();
		

		
		if($this->request->getFile('product_image')->isValid()){
			$data['image'] = $this->Generator->uuid . ".jpg";
			$imagefile = $this->request->getFile('product_image')->store('/products', $data['image']);
		}


		$products = $this->products->save($data);
		
		return $this->respondCreated($products);
	}

	public function edit($id)
	{

		$products = $this->products->select([
			"products.code",
			"products.name",
			"products.description",
			"products.price",
			"products.category_id",
			"products.brand_id",
			"products.image",
			"product_categories.name as category_name",
			"product_brands.name as brand_name",
		])
		->join('product_categories', 'product_categories.id = products.category_id', "LEFT")
		->join('product_brands', 'product_brands.id = products.brand_id', "LEFT")
		->find($id);

		return $this->respondCreated($products);
	}
	
	public function update($id)
	{

		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();
		
		if($this->request->getFile('product_image')->isValid()){
			$data['image'] = $this->Generator->uuid . ".jpg";
			$imagefile = $this->request->getFile('product_image')->store('/products', $data['image']);
		}

		$products = $this->products->update($id, $data);
		
		return $this->respondCreated($products);
	}
	
	public function delete($id)
	{	
		$products = $this->products->delete($id);

		return $this->respondCreated($products);
	}
	
}

