<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddCustomers extends Migration
{
	public function up()
	{
		$this->forge->addField([
			
			'id' => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'code' => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
			],
			'name' => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
			],
			'phone_number' => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
				'null' => true
			 ],
			'email' => [
				'type'           => 'VARCHAR',
				'constraint'     => '191'
			],
			'birth_date' => [
				'type'           => 'DATE',
				'null' => true
			],
			'religion' => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
				'null' => true
			],
			'address' => [
				'type'           => 'TEXT',
				'null' => true
			],
			'postal_code' => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
				'null' => true
			],
			"bank_name" => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
				'null' => true
			],
			"bank_branch" => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
				'null' => true
			],
			"account_name" => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
				'null' => true
			],
			"account_number" => [
				'type'           => 'VARCHAR',
				'constraint'     => '191',
				'null' => true
			],
			'created_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'updated_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'deleted_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null' => true
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null' => true
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null' => true
			]
		]);
			
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('customers');
	}

	public function down()
	{
		$this->forge->dropTable('customers');
	}
}
