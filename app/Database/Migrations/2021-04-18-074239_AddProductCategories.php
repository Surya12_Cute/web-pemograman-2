<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddProductCategories extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'code' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'name' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'description' => [
				'type'           => 'TEXT',
				'null' => true
			],
			'created_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'updated_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'deleted_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null' => true
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null' => true
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null' => true
			]
		]);
			
		
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('product_categories');
	}

	public function down()
	{
		$this->forge->dropTable('product_categories');
	}
}
