<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addtransactions extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id'              => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'invoice'         => ['type' => 'varchar', 'constraint' => 100],
			'customer_id'     => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
			'product_id'      => ['type' => 'int', 'constraint' => 11, 'unsigned' => true],
			'qty'             => ['type' => 'int', 'constraint' => 100,'null' => true],
			'marketing_id'    => ['type' => 'int', 'constraint' => 11, 'null' => true],
			'status'          => ['type' => 'varchar', 'constraint' => 100, 'null' => true],
			'created_at'      => ['type' => 'datetime', 'null' => true],
            'updated_at'      => ['type' => 'datetime', 'null' => true],
            'deleted_at'      => ['type' => 'datetime', 'null' => true],
        ]);
        $this->forge->addKey('id', true);
		$this->forge->createTable('transactions', true);
	}

	public function down()
	{
		$this->forge->dropTable('transactions');
	}
}
