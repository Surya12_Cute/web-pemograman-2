<?php

if(!function_exists('rupiah')) {
    function rupiah($value){
        return "Rp " . number_format($value, 2, ",", ".");
    }
}


if(!function_exists('marketing_user')) {
    function marketing_user(){
        
        $marketings = new \App\Models\marketings();

        $marketing_user = user();
        $marketing_user->marketing = $marketings->find($marketing_user->marketing_id);
        return $marketing_user;
    }
}


if(!function_exists('product_url')) {
    function product_url($image = null){
        return base_url($image ? 'products/' . $image : 'products');
    }
}

if(!function_exists('banner_url')) {
    function banner_url($image = null){
        return base_url($image ? 'banners/' . $image : 'banners');
    }
}

if(!function_exists('brand_url')) {
    function brand_url($image = null){
        return base_url($image ? 'brands/' . $image : 'brands');
    }
}