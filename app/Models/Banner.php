<?php

namespace App\Models;

use CodeIgniter\Model;

class Banner extends Model
{
	protected $table                = 'banner';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $returnType           = 'object';
	protected $useSoftDelete        = true;
	protected $allowedFields        = [
		"name",
		"link",
		"title",
		"description",
		"image"
	];

	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

}
