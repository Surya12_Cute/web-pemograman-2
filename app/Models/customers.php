<?php

namespace App\Models;

use CodeIgniter\Model;

class customers extends Model{

    protected $table = 'customers';


    protected $allowedFields = [
        "id",
        "code",
        "name",
        "phone_number",
        "email",
        "birth_date",
        "religion",
        "address",
        "postal_code",
        "bank_name",
        "bank_branch",
        "account_name",
        "account_number",
        "npwp",
        "nik",
        "created_at",
        "updated_at",
        "deleted_at"
    ];


    protected $primaryKey = 'id';

    
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;


    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';


    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];
  

  protected function beforeInsert(array $data){
    $data['data']['created_at'] = date('Y-m-d H:i:s');
    return $data;
  }

  protected function beforeUpdate(array $data){
    $data['data']['updated_at'] = date('Y-m-d H:i:s');
    return $data;
  }
}