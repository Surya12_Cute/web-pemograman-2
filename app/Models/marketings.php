<?php

namespace App\Models;

use CodeIgniter\Model;
use Myth\Auth\Authorization\GroupModel;
use Myth\Auth\Entities\User;
use Config\Services;

class marketings extends Model{

    protected $table = 'marketings';


    protected $allowedFields = [
        "id",
        "code",
        "name",
        // "email",
        // "password",
        "gender",
        "birth_date",
        "birth_place",
        "phone_number",
        "address",
        "created_at",
        "updated_at",
        "deleted_at"
    ];


    protected $primaryKey = 'id';

    
    protected $returnType     = 'object';
    protected $useSoftDeletes = true;


    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';


    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];
  

  protected function beforeInsert(array $data){
    $data['data']['created_at'] = date('Y-m-d H:i:s');
    return $data;
  }

  protected function beforeUpdate(array $data){
    $data['data']['updated_at'] = date('Y-m-d H:i:s');
    return $data;
  }


  public function CreateMarketingUser($data) {

      $authorize = Services::authorization();

      $this->insert([
        'code' => $data['code'],
        'name' => $data['name'],
        'birth_date' => $data['birth_date'] ?? null,
        'birth_place' => $data['birth_place'] ?? null,
        'gender' => $data['gender'] ?? null,
        'phone_number' => $data['phone_number'] ?? null
      ]);

      $user = new User([
        'username' => $data['code'],
        'marketing_id' => $this->InsertId(),
        'email' => $data['email'],
        'password' => $data['password'],
      ]);

      $user->activate();
      $users = model('UserModel');
      $users->insert($user);

      $authorize->addUserToGroup($users->InsertId(), $data['role_id']);

      return true;
  }


  public function UpdateMarketingUser($id,$data) {
  

      $this->update($id, [
        'code' => $data->code,
        'name' => $data->name,
        'birth_date' => $data->birth_date ?? null,
        'birth_place' => $data->birth_place ?? null,
        'gender' => $data->gender ?? null,
        'phone_number' => $data->phone_number ?? null
      ]);

    

      $users = model('UserModel');
      $user = $users->where('marketing_id', $id)->first();

      
      if (is_null($user))
      {
        throw new \RuntimeException("User Marketing Not Found");
      }


      try {
        if($data->password){
          $user->password 	 = $data->password;
        }
          $user->username 	 = $data->code;
          $user->email 		   = $data->email;
          
          $users->save($user);
      } catch (\Exception $e) {
        log_message('error', $e);
      }

      $this->db->table('auth_groups_users')->where(['user_id' => $user->id])->delete();
      Services::authorization()->addUserToGroup($user->id, $data->role_id);


      return true;
      

          
      
  }
}