<?php

namespace App\Models;

use CodeIgniter\Model;

class menus extends Model{

    protected $table = 'menus';


    protected $allowedFields = [
        "id",
        "name",
        "icon",
        "link"
    ];


    protected $primaryKey = 'id';

    public function all_withFullPath()
    {
        return $this->select([
            "name", "icon",
            "link as link_name",
            "CONCAT( '" . base_url("admin") . "', link) as link"
        ])->findAll();
    }

}