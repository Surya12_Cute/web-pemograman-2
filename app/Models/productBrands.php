<?php

namespace App\Models;

use CodeIgniter\Model;

class productBrands extends Model
{
	protected $table                = 'product_brands';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $returnType           = 'object';
	protected $useSoftDelete        = true;
	protected $allowedFields        = [
		"name",
		"code",
		"distributor_link",
		"description",
		"image"
	];

	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

}
