<?php

namespace App\Models;

use CodeIgniter\Model;

class productCategories extends Model{

    protected $table = 'product_categories';


    protected $allowedFields = [
        "id",
        "code",
        "name",
        "description",
        "created_by",
        "updated_by",
        "deleted_by",
        "created_at",
        "updated_at",
        "deleted_at"
    ];


    protected $primaryKey = 'id';

    
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;


    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';


    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];
    protected $beforeDelete = ['beforeDelete'];
  

  protected function beforeInsert(array $data){
    $data['data']['created_at'] = date('Y-m-d H:i:s');
    return $data;
  }

  protected function beforeUpdate(array $data){
    $data['data']['updated_at'] = date('Y-m-d H:i:s');
    return $data;
  }
  
  protected function beforeDelete(array $data){
    // $data['data']['updated_at'] = date('Y-m-d H:i:s');
    return $data;
  }
}