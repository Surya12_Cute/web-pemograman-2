<?php

namespace App\Models;

use CodeIgniter\Model;

class role extends Model
{
    protected $table = 'auth_groups';

    
    protected $allowedFields = [
        "name",
        "description"
    ];

    protected $primaryKey = 'id';
}
