<script>

    $(document).ready(function() {

        $(document).on("click","._edit",function() {

            ModalCustomer.modal('show');
            ModalCustomer.find(".overlay").show();
            ModalCustomer.find('.modal-title').html('Edit Customer');
            ModalCustomer.find('#list_error').html("");
            ModalCustomer.find('form').attr('action', `/admin/customers/${$(this).data('id')}/update`);
            ModalCustomer.find('form').attr('method', `PUT`);

            const Axios = axios.get(`/admin/customers/${$(this).data('id')}/edit`);
            Axios.then((response) => {
                ModalCustomer.find("#code").val(response.data.code);
                ModalCustomer.find("#name").val(response.data.name);
                ModalCustomer.find("#phone_number").val(response.data.phone_number);
                ModalCustomer.find("#email").val(response.data.email);
                ModalCustomer.find("#address").val(response.data.address);
                ModalCustomer.find("#birth_date").val(response.data.birth_date);
                ModalCustomer.find("#religion").val(response.data.religion).change();

                setTimeout(() => ModalCustomer.find(".overlay").hide(), 200);
            });
            Axios.catch((error) => {
                console.log(error);
                ModalCustomer.find(".overlay").hide()
            });
        });


        $(document).on("click","._create",function() {

            ModalCustomer.modal('show');

            ModalCustomer.find(".overlay").show();
            ModalCustomer.find('.modal-title').html('Create Customer');
            ModalCustomer.find('form').attr('action', `/admin/customers/store`);
            ModalCustomer.find('form').attr('method', `POST`);

            ModalCustomer.find('#list_error').html("");

            ModalCustomer.find("#code").val("");
            ModalCustomer.find("#name").val("");
            ModalCustomer.find("#phone_number").val("");
            ModalCustomer.find("#email").val("");
            ModalCustomer.find("#address").val("");
            ModalCustomer.find("#birth_date").val("");
            ModalCustomer.find("#religion").val("").change();

            setTimeout(() => ModalCustomer.find(".overlay").hide(), 100);
        });

          
        ModalCustomer.find('form').on('submit', function(e) {
            e.preventDefault();
            ModalCustomer.find('#list_error').html("");
            ModalCustomer.find(".overlay").show();
            $form =  $(this);
            const Axios = axios({
              method: $form.attr('method'),
              url: $form.attr('action'),
              data: objectifyForm($form.serializeArray())
            });

              Axios.then((response) => {              
                Toast.fire({
                  icon: 'success',
                  title: 'Customer Berhasil Di Update'
                })
                ModalCustomer.modal('hide');
                reloadTable();
              });
              Axios.catch((error) => {
                if(error.response.status == 400){
                  setTimeout(() => ModalCustomer.find(".overlay").hide(), 100);
                  ModalCustomer.find('#list_error').append($.parseHTML(error.response.data));
                  Toast.fire({
                    icon: 'warning',
                    title: 'Harap Cek Kembali Data yang Dimasukkan !'
                  })

                } else {
                  ModalCustomer.modal('hide');
                  Toast.fire({
                    icon: 'error',
                    title: 'Terjadi Kendala teknisi'
                  })

                }
              });
        });

        $(document).on("click","._delete",function() {
            Swal.fire({
              title: 'Apakah Anda Yakin?',
              text: "Customer ini akan di hapus secara Permanen!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result.isConfirmed) {

                const Axios = axios.post(`/admin/customers/${$(this).data('id')}/delete`);
                Axios.then((response) => {

                      
                    Swal.fire(
                      'Terhapus!',
                      'Customer Ini Terhapus.',
                      'success'
                    )
                    reloadTable();
                    
                });
                Axios.catch((error) => {
                    console.log(error);
                });

              }else{
                  Swal.fire({
                    icon: 'error',
                    text: 'Dibatalkan'
                  })
              }
            })

        });

    });

</script>