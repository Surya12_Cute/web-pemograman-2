<script>

    $(document).ready(function() {

        $(document).on("click","._edit",function() {

            ModalMarketing.modal('show');
            ModalMarketing.find(".overlay").show();
            ModalMarketing.find('.modal-title').html('Edit Marketing');
            ModalMarketing.find('#list_error').html("");
            ModalMarketing.find('form').attr('action', `/admin/marketings/${$(this).data('id')}/update`);
            ModalMarketing.find('form').attr('method', `PUT`);

            const Axios = axios.get(`/admin/marketings/${$(this).data('id')}/edit`);
            Axios.then((response) => {
             
                ModalMarketing.find("#code").val(response.data.code);
                ModalMarketing.find("#name").val(response.data.name);
                ModalMarketing.find("#phone_number").val(response.data.phone_number);
                ModalMarketing.find("#password").val("");
                ModalMarketing.find("#email").val(response.data.email);
                ModalMarketing.find("#birth_place").val(response.data.birth_place);
                ModalMarketing.find("#birth_date").val(response.data.birth_date);
                ModalMarketing.find("#role_id").val(response.data.role_id).change();

                setTimeout(() => ModalMarketing.find(".overlay").hide(), 200);
            });
            Axios.catch((error) => {
                console.log(error);
                ModalMarketing.find(".overlay").hide()
            });
        });


        $(document).on("click","._create",function() {

            ModalMarketing.modal('show');

            ModalMarketing.find(".overlay").show();
            ModalMarketing.find('.modal-title').html('Create Marketing');
            ModalMarketing.find('form').attr('action', `/admin/marketings/store`);
            ModalMarketing.find('form').attr('method', `POST`);

            ModalMarketing.find('#list_error').html("");

            ModalMarketing.find("#code").val("");
            ModalMarketing.find("#name").val("");
            ModalMarketing.find("#phone_number").val("");
            ModalMarketing.find("#email").val("");
            ModalMarketing.find("#birth_place").val("");
            ModalMarketing.find("#birth_date").val("");
            ModalMarketing.find("#password").val("");
            ModalMarketing.find("#gender").val("").change();

            setTimeout(() => ModalMarketing.find(".overlay").hide(), 100);
        });

          
        ModalMarketing.find('form').on('submit', function(e) {
            e.preventDefault();
            ModalMarketing.find('#list_error').html("");
            ModalMarketing.find(".overlay").show();
            $form =  $(this);
            const Axios = axios({
              method: $form.attr('method'),
              url: $form.attr('action'),
              data: objectifyForm($form.serializeArray())
            });

              Axios.then((response) => {              
                Toast.fire({
                  icon: 'success',
                  title: 'Marketing Berhasil Di Update'
                })
                ModalMarketing.modal('hide');
                reloadTable();
              });
              Axios.catch((error) => {
                if(error.response.status == 400){
                  setTimeout(() => ModalMarketing.find(".overlay").hide(), 100);
                  ModalMarketing.find('#list_error').append($.parseHTML(error.response.data));
                  Toast.fire({
                    icon: 'warning',
                    title: 'Harap Cek Kembali Data yang Dimasukkan !'
                  })

                } else {
                  ModalMarketing.modal('hide');
                  Toast.fire({
                    icon: 'error',
                    title: 'Terjadi Kendala teknisi'
                  })

                }
              });
        });

        $(document).on("click","._delete",function() {
            Swal.fire({
              title: 'Apakah Anda Yakin?',
              text: "Marketing ini akan di hapus secara Permanen!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result.isConfirmed) {

                const Axios = axios.post(`/admin/marketings/${$(this).data('id')}/delete`);
                Axios.then((response) => {

                      
                    Swal.fire(
                      'Terhapus!',
                      'Marketing Ini Terhapus.',
                      'success'
                    )
                    reloadTable();
                    
                });
                Axios.catch((error) => {
                    console.log(error);
                });

              }else{
                  Swal.fire({
                    icon: 'error',
                    text: 'Dibatalkan'
                  })
              }
            })

        });

    });

</script>