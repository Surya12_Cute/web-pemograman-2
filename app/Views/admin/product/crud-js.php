<script>

    $(document).ready(function() {

        $(document).on("click","._edit",function() {

            ModalProducts.find("#product_image").closest(".imgUp").find('.imagePreview').css("background-image", "url()");
            ModalProducts.find("#product_image").val("");

            ModalProducts.modal('show');
            ModalProducts.find(".overlay").show();
            ModalProducts.find('.modal-title').html('Edit Produk');
            ModalProducts.find('#list_error').html("");
            ModalProducts.find('form').attr('action', `/admin/products/${$(this).data('id')}/update`);
            ModalProducts.find('form').attr('method', `PUT`);

            const Axios = axios.get(`/admin/products/${$(this).data('id')}/edit`);
            Axios.then((response) => {
             
                ModalProducts.find("#code").val(response.data.code);
                ModalProducts.find("#name").val(response.data.name);
                ModalProducts.find("#description").val(response.data.description);
                ModalProducts.find("#price").val(response.data.price);
                ModalProducts.find("#category_id").append(
                    `<option selected value="${response.data.category_id}">${response.data.category_name}</option>`
                ).trigger('change');

                ModalProducts.find("#brand_id").append(
                    `<option selected value="${response.data.brand_id}">${response.data.brand_name}</option>`
                ).trigger('change');


                // response.data
                if(response.data.image){
                  ModalProducts.find("#product_image").closest(".imgUp").find('.imagePreview').css("background-image", "url(<?= product_url() ?>/"+response.data.image+")");
                }
                

                setTimeout(() => ModalProducts.find(".overlay").hide(), 200);
            });
            Axios.catch((error) => {
                console.log(error);
                ModalProducts.find(".overlay").hide()
            }); 
        });


        $(document).on("click","._create",function() {

            ModalProducts.modal('show');

            ModalProducts.find(".overlay").show();
            ModalProducts.find('.modal-title').html('Create Product');
            ModalProducts.find('form').attr('action', `/admin/products/store`);
            ModalProducts.find('form').attr('method', `POST`);

            ModalProducts.find('#list_error').html("");

            ModalProducts.find("#product_image").closest(".imgUp").find('.imagePreview').css("background-image", "url()");
            ModalProducts.find("#product_image").val("");
            ModalProducts.find("#code").val("");
            ModalProducts.find("#name").val("");
            ModalProducts.find("#description").val("");
            ModalProducts.find("#price").val(0);
            ModalProducts.find("#category_id").val("").change();
            ModalProducts.find("#brand_id").val("").change();

            setTimeout(() => ModalProducts.find(".overlay").hide(), 100);
        });

          
        ModalProducts.find('form').on('submit', function(e) {
            e.preventDefault();
            ModalProducts.find('#list_error').html("");
            ModalProducts.find(".overlay").show();
            const $form = $(e.target);
            var form_data = new FormData($form[0]);
            const Axios = axios.post($form.attr('action'), form_data);

              Axios.then((response) => {              
                Toast.fire({
                  icon: 'success',
                  title: 'Marketing Berhasil Di Update'
                })
                ModalProducts.modal('hide');
                reloadTable();
              });
              Axios.catch((error) => {
                if(error.response.status == 400){
                  setTimeout(() => ModalProducts.find(".overlay").hide(), 100);
                  ModalProducts.find('#list_error').append($.parseHTML(error.response.data));
                  Toast.fire({
                    icon: 'warning',
                    title: 'Harap Cek Kembali Data yang Dimasukkan !'
                  })

                } else {
                  ModalProducts.modal('hide');
                  Toast.fire({
                    icon: 'error',
                    title: 'Terjadi Kendala teknisi'
                  })

                }
              });
        });

        $(document).on("click","._delete",function() {
            Swal.fire({
              title: 'Apakah Anda Yakin?',
              text: "Marketing ini akan di hapus secara Permanen!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result.isConfirmed) {

                const Axios = axios.post(`/admin/products/${$(this).data('id')}/delete`);
                Axios.then((response) => {

                      
                    Swal.fire(
                      'Terhapus!',
                      'Marketing Ini Terhapus.',
                      'success'
                    )
                    reloadTable();
                    
                });
                Axios.catch((error) => {
                    console.log(error);
                });

              }else{
                  Swal.fire({
                    icon: 'error',
                    text: 'Dibatalkan'
                  })
              }
            })

        });


        $('#category_id').select2({
          allowClear: false,
          ajax: {
              url: "<?= base_url('/admin/category-product/select') ?>",
              dataType: 'json',
              delay: 0,
              data: function (params) {
                return {
                  search: params.term
                };
              },
              processResults: function (data) {
                return {
                  results: data
                };
              },
              cache: false,
          },
          placeholder: 'Produk Kategori',
        });


        $('#brand_id').select2({
          allowClear: false,
          ajax: {
              url: "<?= base_url('/admin/brand-product/select') ?>",
              dataType: 'json',
              delay: 0,
              data: function (params) {
                return {
                  search: params.term
                };
              },
              processResults: function (data) {
                return {
                  results: data
                };
              },
              cache: false,
          },
          placeholder: 'Produk Brand',
        });



      $(document).on("change",".uploadFile", function() {
          var uploadFile = $(this);
          var files = !!this.files ? this.files : [];
          if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
  
          if (/^image/.test( files[0].type)){ // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
  
              reader.onloadend = function(){ // set image data as background of div
                  uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
              }
          }
        
      });


    });

</script>
