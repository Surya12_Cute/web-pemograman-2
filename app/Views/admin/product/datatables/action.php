<a href="#" class="btn btn-flat btn-xs btn-info _edit" data-id='<?= $id ?>'>
    <i class="fa fa-edit"></i>
</a>

<a href="#" class="btn btn-flat btn-xs btn-danger _delete" data-id='<?= $id ?>'>
    <i class="fa fa-trash"></i>
</a>