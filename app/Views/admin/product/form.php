
    <div class="row d-flex justify-content-center">
        <div class="col-sm-5 imgUp">
        <div class="imagePreview"></div>
            <label class="btn btn-primary"> Upload
                <input type="file" class="uploadFile img" value="Upload Photo" name="product_image" id="product_image"  style="width:0%; height:0%; overflow: hidden;">
            </label>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
            <label>Kode Produk</label>
                <input type="text" class="form-control" placeholder="Kode" name='code' id ='code'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Nama Produk</label>
                <input type="text" class="form-control" placeholder="Nama" name='name' id ='name'>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Kategori</label>
                <select class="select2" style="width: 100%;" name='category_id' id ='category_id'>
                    <option selected value =''></option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Merek</label>
                <select class="select2" style="width: 100%;" name='brand_id' id ='brand_id'>
                    <option selected value =''></option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <label>Harga</label>
                <input type="text" class="form-control" placeholder="Harga" name='price' id ='price' value ='0'>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" rows="3" placeholder="Deskripsi" name='description' id ='description'></textarea>
            </div>
        </div>
    </div>
    