
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
            <label>Invoice</label>
                <input type="text" class="form-control" placeholder="Invoice" name='invoice' id ='invoice'>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>Produk</label>
                <select class="select2" style="width: 100%;" name='product_id' id ='product_id'>
                    <option selected value =''></option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="row">
        
        <div class="col-md-6">
            <div class="form-group">
                <label>Customer</label>
                <select class="select2" style="width: 100%;" name='customer_id' id ='customer_id'>
                    <option selected value =''></option>
                </select>
            </div>
        </div>

        
        <div class="col-sm-3">
            <div class="form-group">
            <label>Quantity</label>
                <input type="number" class="form-control" placeholder="Qty" name='qty' id ='qty'>
            </div>
        </div>
        
    </div>
    
    <div class="row">
        <!-- <div class="col-md-6">
            <div class="form-group">
                <label>Di Handel Oleh</label>
                <select class="select2" style="width: 100%;" name='marketing_id' id ='marketing_id'>
                    <option selected value =''></option>
                </select>
            </div>
        </div> -->
        
        <div class="col-md-6">
            <div class="form-group">
                <label>Status Transaksi</label>
                <select class="form-control select2" style="width: 100%;" name='status' id ='status'>
                    <option value="pending" selected>Menunggu</option>
                    <option value="approved">Disetujui</option>
                    <option value="rejected">Ditolak</option>
                </select>
            </div>
        </div>
    </div>
