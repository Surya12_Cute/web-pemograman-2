  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="<?=base_url('admin')?>" class="brand-link">
      <img src="<?=base_url('assets/template/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Web Pemograman II</span>
    </a>


    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=base_url('assets/template/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= marketing_user()->marketing->name ?></a>
        </div>
      </div>

    <?= view('layout/menu') ?>
    </div>
  </aside>
