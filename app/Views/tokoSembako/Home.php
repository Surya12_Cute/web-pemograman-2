<?= $this->extend('tokoSembako/layout/app') ?>

<?= $this->section('content') ?>

    <!-- Carousel -->
    <div id="carouselExampleControls" class="carousel slide mt-5" data-ride="carousel">
      <div class="carousel-inner">
        <div class="container">

          <?php foreach ( $banners  as  $key => $baner ) { ?>
            <div class="carousel-item <?= $key == 0 ? 'active': '' ?>">
              <div class="row pt-5 justify-content-center">
                <div class="col-9 col-sm-4 col-md-6 col-lg-5">
                  <h1 class="mb-4">
                    <?= $baner->title ?>
                  </h1>
                  <p class="mb-4"> 
                    <?= $baner->description ?>
                  </p>
                  <a href="<?= $baner->link ?>" class="btn btn-warning text-white">Get It Now</a>
                </div>
                <div class="col-3 col-sm-6 col-md-4 col-lg-4 d-none d-sm-block offset-1">
                  <img src="<?= banner_url($baner->image)  ?>" class="img-fluid">
                </div>
              </div>
            </div>
          <?php } ?>

        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div> 
    <!-- Akhir Carousel -->

    <!-- Brands -->
    <section>
      <div class="container">
        <div class="row p-5 text-center">
          <div class="col-md">
            <img src="img/cc.png" class="img-fluid">
          </div>
          <div class="col-md">
            <img src="img/nike.png" class="img-fluid">
          </div>
          <div class="col-md">
            <img src="img/pnb.png" class="img-fluid">
          </div>
          <div class="col-md">
            <img src="img/uniqlo.png" class="img-fluid">
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir Brands -->

    <!-- Features -->
    <section class="features bg-light p-5">
      <div class="container">
        <div class="row m-3">
          <div class="col">
            <h3>Spesial Chirstmas</h3>
            <p>Spesial Pakaian Menyambut Natal</p>
          </div>
        </div>

        <div class="row">
          <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <figure class="figure">
              <div class="figure-img">
              <img src="img/model1.png" class="figure-img img-fluid">
              <a href="" class="d-flex justify-content-center">
                <img src="img/badge.png" class="align-self-center">
              </a>
              </div>
              <figcaption class="figure-caption text-center">
               <h5>Jeans Pubb.</h5> 
              <p>IDR 290.000</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <figure class="figure">
              <div class="figure-img">
                <img src="img/model1.png" class="figure-img img-fluid">
                <a href="" class="d-flex justify-content-center">
                  <img src="img/badge.png" class="align-self-center">
                </a>
              </div>
              <figcaption class="figure-caption text-center">
               <h5>Jeans Pubb.</h5> 
              <p>IDR 290.000</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <figure class="figure">
              <div class="figure-img">
                <img src="img/model1.png" class="figure-img img-fluid">
                <a href="" class="d-flex justify-content-center">
                  <img src="img/badge.png" class="align-self-center">
                </a>
              </div>
              <figcaption class="figure-caption text-center">
               <h5>Jeans Pubb.</h5> 
              <p>IDR 290.000</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <figure class="figure">
              <div class="figure-img">
                <img src="img/model1.png" class="figure-img img-fluid">
                <a href="" class="d-flex justify-content-center">
                  <img src="img/badge.png" class="align-self-center">
                </a>
              </div>
              <figcaption class="figure-caption text-center">
               <h5>Jeans Pubb.</h5> 
              <p>IDR 290.000</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <figure class="figure">
              <div class="figure-img">
                <img src="img/model1.png" class="figure-img img-fluid">
                <a href="" class="d-flex justify-content-center">
                  <img src="img/badge.png" class="align-self-center">
                </a>
              </div>
              <figcaption class="figure-caption text-center">
               <h5>Jeans Pubb.</h5> 
              <p>IDR 290.000</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-4 col-md-3 col-lg-2">
            <figure class="figure">
              <div class="figure-img">
                <img src="img/model1.png" class="figure-img img-fluid">
                <a href="" class="d-flex justify-content-center">
                  <img src="img/badge.png" class="align-self-center">
                </a>
              </div>
              <figcaption class="figure-caption text-center">
               <h5>Jeans Pubb.</h5> 
              <p>IDR 290.000</p>
            </figcaption>
            </figure>
          </div>
        </div>
        </div>
      </section>
   <!-- Akhir Feartures -->

    <!-- Designer -->
    <section class="designer p-3">
      <div class="container">
        <div class="row m-3">
          <div class="col">
            <h3>Our Designer</h3>
            <p>Dapatkan Pakian Dari Desigener Favorite Kamu</p>
          </div>
        </div>
        
        <div class="row">
          <div class="col-6 col-sm-3 text-center p-3">
            <figure class="figure">
              <img src="img/design1.png" class="figure-img img-fluid">
              <figcaption class="figure-caption text-center">
               <h5>Anne Mortgery</h5> 
               <p>Artistic Cloth</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-3 text-center p-3">
            <figure class="figure">
              <img src="img/design1.png" class="figure-img img-fluid">
              <figcaption class="figure-caption text-center">
               <h5>Anne Mortgery</h5> 
               <p>Artistic Cloth</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-3 text-center p-3">
            <figure class="figure">
              <img src="img/design1.png" class="figure-img img-fluid">
              <figcaption class="figure-caption text-center">
               <h5>Anne Mortgery</h5> 
               <p>Artistic Cloth</p>
            </figcaption>
            </figure>
          </div>

          <div class="col-6 col-sm-3 text-center p-3">
            <figure class="figure">
              <img src="img/design1.png" class="figure-img img-fluid">
              <figcaption class="figure-caption text-center">
               <h5>Anne Mortgery</h5> 
               <p>Artistic Cloth</p>
            </figcaption>
            </figure>
          </div>

          <div class="col text-center">
            <a href="">See All Our Designers</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir Designer -->



<?= $this->endSection() ?>
