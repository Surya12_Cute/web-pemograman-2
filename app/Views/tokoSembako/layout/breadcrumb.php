<?php if (current_url(true)->getPath() != "/") {?>
<!-- Breadcrumbs -->
<div class="container mt-5">
    <nav>
        <ol class="breadcrumb bg-transparent p-0">
          <li class="breadcrumb-item"><a href="#">Category</a></li>
          <li class="breadcrumb-item active" aria-current="page">Single Product</li>
        </ol>
      </nav>
</div>
<!-- Akhir Breadcrumbs -->
<?php } ?>